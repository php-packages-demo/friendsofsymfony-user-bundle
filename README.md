# friendsofsymfony/[user-bundle](https://packagist.org/packages/friendsofsymfony/user-bundle)

User management compatible with custom storages

# Obsolated
* [*Basic Authentication and Registration Steps with Symfony Security Bundle (Symfony 5)*
  ](https://medium.com/suleyman-aydoslu/basic-authentication-and-registration-steps-with-symfony-security-bundle-symfony-5-a5518ee0a9da)
  2020-05 Süleyman Aydoslu
* [*Do not use FOSUserBundle*](https://jolicode.com/blog/do-not-use-fosuserbundle)
  2015-12
* (fr) [*Gérer les utilisateurs dans son application Symfony sans le FOSUserBundle*](https://blog.netinfluence.ch/2019/05/10/gerer-les-utilisateurs-dans-son-application-symfony-sans-le-fosuserbundle/) 2019 Romaric
